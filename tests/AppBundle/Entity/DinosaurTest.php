<?php
namespace Tests\AppBundle\Entity;
use AppBundle\Entity\Dinosaur;
use PHPUnit\Framework\TestCase;

class DinosaurTest extends TestCase
{

    public function testSettingLength() {
        $dinosaur = new Dinosaur();
        $this->assertSame(0,$dinosaur->getLength());
        $dinosaur->setLength(7);
        $this->assertSame(7,$dinosaur->getLength());
    }

    public function testDinosaurHasNotShrunk() {
        $dinosaur = new Dinosaur();
        $dinosaur->setLength(15);
        $this->assertGreaterThan('13', $dinosaur->getLength(),'Did you put dinosaur in washing machine');
    }

    public function testReturnsFullSpecificationOfDinosaur() {
        $dinosaur = new Dinosaur();

        $this->assertSame('The Unknown non-carnivorous dinosaur is 0 meters long', $dinosaur->getSpecification());

    }

    /**
     * Each Dinosaur to have agenus - like Tyrannosaurus - and a flag that says whether or not it
     * likes to eat people, means whether or not it's carnivorous.
     *
     */
    public function testReturnsFullSpecificationForTyrannosaurus() {
        $dinosaur = new Dinosaur('Tyrannosaurus', true);
        $dinosaur->setLength(12);
        $this->assertSame(
            'The Tyrannosaurus carnivorous dinosaur is 12 meters long',
            $dinosaur->getSpecification()
        );
    }
}